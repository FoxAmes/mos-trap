# MOS-Trap
A simple debugging tool for 6502-based computers, such as [Ben Eater's](https://eater.net/6502).

![](assets/demo.webm)

## Features
|Feature|Status|Notes|
|-|-|-|
|Data and Address bus view|✅||
|Clock stepping|✅||
|History trimming|✅||
|Opcode translation|☑️|Clock cycles per instruction and operands not implemented|
|HAL|☑️|Only raspberry pi implemented via `embedded-hal`|
|Details|❌|[Issue #1](https://gitlab.com/FoxAmes/mos-trap/-/issues/1)|

## Setup and Usage
Check out the [wiki](https://gitlab.com/FoxAmes/mos-trap/-/wikis/home) for setup details.

Usage is fairly straightforward: press `c` to manually trigger a clock pulse, `t` to trim the history up to the selected entry, and `q` to close the application.