use crate::{
    mos::{ADDR_WIDTH, DATA_WIDTH},
    Pin,
};
use anyhow::{anyhow, Context};

pub struct HalData {
    gpio: rppal::gpio::Gpio,
}

impl HalData {
    pub fn new() -> Result<Self, anyhow::Error> {
        let gpio = rppal::gpio::Gpio::new()?;
        Ok(Self { gpio })
    }

    pub fn get_addr_pins(&self) -> Result<[Pin; ADDR_WIDTH], anyhow::Error> {
        // Use predefined pin numbers for the address bus
        const ADDR_PIN_NUMS: [u8; ADDR_WIDTH] =
            [9, 25, 11, 8, 7, 0, 1, 5, 6, 12, 13, 19, 16, 26, 20, 21];
        // Attempt to fetch every pin from the rpi GPIO, by pin number, sequentially
        Ok(ADDR_PIN_NUMS
            .map(|p| {
                self.gpio
                    .get(p)
                    .context("Error acquiring GPIO pin for address bus")
                    .and_then(|p| Ok(p.into_io(rppal::gpio::Mode::Input)))
            })
            .into_iter()
            .collect::<Result<Vec<_>, _>>()?
            .try_into()
            .or(Err(anyhow!(
                "Failed to reconstruct Pin array after ensuring no errors in acquisition"
            )))?)
    }

    pub fn get_data_pins(&self) -> Result<[Pin; DATA_WIDTH], anyhow::Error> {
        // Use predefined pin numbers for the data bus
        const DATA_PIN_NUMS: [u8; DATA_WIDTH] = [14, 15, 17, 18, 27, 22, 23, 24];
        // Attempt to fetch every pin from the rpi GPIO, by pin number, sequentially
        Ok(DATA_PIN_NUMS
            .map(|p| {
                self.gpio
                    .get(p)
                    .context("Error acquiring GPIO pin for data bus")
                    .and_then(|p| Ok(p.into_io(rppal::gpio::Mode::Input)))
            })
            .into_iter()
            .collect::<Result<Vec<_>, _>>()?
            .try_into()
            .or(Err(anyhow!(
                "Failed to reconstruct Pin array after ensuring no errors in acquisition"
            )))?)
    }

    pub fn get_clock_pin(&self) -> Result<Pin, anyhow::Error> {
        Ok(self
            .gpio
            .get(4)
            .context("Error acquiring GPIO pin for clock")?
            .into_io(rppal::gpio::Mode::Output))
    }
}
