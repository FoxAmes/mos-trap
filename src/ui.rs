use std::{
    cmp::min,
    io::stdout,
    sync::{
        mpsc::{channel, Receiver, Sender},
        Arc, Mutex,
    },
    thread::JoinHandle,
    time::{Duration, Instant},
};

use anyhow::anyhow;
use crossterm::{
    event::{KeyCode, KeyEventKind, KeyModifiers},
    terminal::{self, EnterAlternateScreen, LeaveAlternateScreen},
    ExecutableCommand,
};
use ratatui::{
    backend::{Backend, CrosstermBackend},
    layout::{Constraint, Direction, Layout, Margin},
    style::{Color, Style},
    symbols,
    widgets::{self, Block, Borders, List, ListItem, ListState, Scrollbar, ScrollbarState},
    Terminal,
};

use crate::mos::opcodes::{OPCODE_CYCLES, OPCODE_NAMES, OPCODE_WIDTH};

static BLOCK_STYLE: Style = Style::new()
    .fg(Color::Rgb(0x87, 0x39, 0x0C))
    .bg(Color::Rgb(0x16, 0x10, 0x32));

static TEXT_STYLE: Style = Style::new()
    .fg(Color::Rgb(0xED, 0x7D, 0x3A))
    .bg(Color::Rgb(0x16, 0x10, 0x32));

static HIGHLIGHT_STYLE: Style = Style::new()
    .fg(Color::Rgb(0xF4, 0xEE, 0xA9))
    .bg(Color::Rgb(0x2B, 0x26, 0x45));

pub enum TUIState {
    MemoryView {
        memory_history: Vec<(u8, u16)>,
        active_history: usize,
    },
    GraphView,
}

pub enum TUIInputEvent {
    Clock,
}

pub struct MOSTrapTUI {
    pub state: TUIState,
    refresh_rate: f64,
    last_frame_time: Instant,
    needs_cleanup: bool,
    pub should_close: bool,
}

impl MOSTrapTUI {
    pub fn new() -> anyhow::Result<(
        Arc<Mutex<Self>>,
        JoinHandle<anyhow::Result<()>>,
        Receiver<TUIInputEvent>,
    )> {
        // Initialize terminal
        stdout().execute(EnterAlternateScreen)?;
        terminal::enable_raw_mode()?;
        let needs_cleanup = true; // We need to undo raw mode and alt screen once the TUI is dropped
        let mut terminal = Terminal::new(CrosstermBackend::new(stdout()))?;
        terminal.clear()?;

        // Init TUI data
        let state = TUIState::MemoryView {
            memory_history: vec![],
            active_history: 0,
        };
        let tui = Arc::new(Mutex::new(Self {
            state,
            refresh_rate: 60f64,
            last_frame_time: std::time::Instant::now(),
            needs_cleanup,
            should_close: false,
        }));
        let tui_render = tui.clone();

        // Create channel to handle events
        let (event_in, event_out) = channel::<TUIInputEvent>();

        // Spawn render loop
        let handle = std::thread::spawn(move || Self::render(terminal, tui_render, event_in));

        Ok((tui, handle, event_out))
    }

    fn render<B>(
        mut terminal: Terminal<B>,
        tui: Arc<Mutex<Self>>,
        event_in: Sender<TUIInputEvent>,
    ) -> anyhow::Result<()>
    where
        B: Backend,
    {
        // Render until close flag is set
        while !tui
            .lock()
            .or(Err(anyhow::anyhow!("Error locking UI to check close flag")))?
            .should_close
        {
            // Handle input
            loop {
                match crossterm::event::poll(Duration::from_nanos(1)) {
                    Ok(true) => match crossterm::event::read()? {
                        crossterm::event::Event::Key(k) => {
                            // Handle quit
                            if k.code == KeyCode::Char('q') && k.kind == KeyEventKind::Press
                                || k.code == KeyCode::Char('c')
                                    && k.kind == KeyEventKind::Press
                                    && k.modifiers.contains(KeyModifiers::CONTROL)
                            {
                                let should_close: &mut bool = &mut tui
                                    .lock()
                                    .or(Err(anyhow!("Error locking UI to quit")))?
                                    .should_close;
                                if !*should_close {
                                    *should_close = true
                                } else {
                                    panic!("Multiple quit requests, hard crashing.")
                                }
                            }
                            // Handle clock
                            else if k.code == KeyCode::Char('c') && k.kind == KeyEventKind::Press
                            {
                                event_in.send(TUIInputEvent::Clock)?;
                            }
                            // Handle history trim
                            else if k.code == KeyCode::Char('t') && k.kind == KeyEventKind::Press
                            {
                                if let TUIState::MemoryView {
                                    memory_history,
                                    active_history,
                                } = &mut tui
                                    .lock()
                                    .or(Err(anyhow!("Error locking menu during trim")))?
                                    .state
                                {
                                    memory_history.drain(..*active_history);
                                    *active_history = 0;
                                }
                            }
                            // Handle list scrolling
                            else if (k.code == KeyCode::Up || k.code == KeyCode::Down)
                                && k.kind == KeyEventKind::Press
                            {
                                if let TUIState::MemoryView {
                                    memory_history,
                                    active_history,
                                } = &mut tui
                                    .lock()
                                    .or(Err(anyhow!("Error locking menu during scroll")))?
                                    .state
                                {
                                    if k.code == KeyCode::Down {
                                        *active_history =
                                            min(memory_history.len() - 1, *active_history + 1);
                                    } else {
                                        *active_history = active_history.saturating_sub(1)
                                    }
                                }
                            }
                        }
                        _ => {}
                    },
                    _ => break,
                }
            }

            // Wait until next frame
            let last_frame_time = tui
                .lock()
                .or(Err(anyhow!("Error locking UI waiting for next frame")))?
                .last_frame_time;
            let refresh_rate = tui
                .lock()
                .or(Err(anyhow!("Error locking UI waiting for next frame")))?
                .refresh_rate;
            let to_wait = std::time::Duration::from_secs_f64(1f64 / refresh_rate)
                - std::time::Duration::default()
                    .saturating_sub(std::time::Instant::now() - last_frame_time);
            std::thread::sleep(to_wait);

            // Start rendering frame
            tui.lock()
                .or(Err(anyhow!("Error locking UI to update frame time")))?
                .last_frame_time = std::time::Instant::now();

            Self::render_state(
                &mut terminal,
                &tui.lock()
                    .or(Err(anyhow!("Error locking UI to render")))?
                    .state,
            );
        }
        Ok(())
    }

    /// Render a frame according to the state
    fn render_state<B>(terminal: &mut Terminal<B>, state: &TUIState)
    where
        B: Backend,
    {
        terminal
            .draw(|frame| {
                match state {
                    TUIState::MemoryView {
                        memory_history,
                        active_history,
                    } => {
                        // Create main layout (memory view | description)
                        let root_layout = Layout::default()
                            .direction(Direction::Vertical)
                            .constraints(vec![Constraint::Min(8), Constraint::Max(1)])
                            .split(frame.size());
                        // Populate info
                        let info_widget =
                            widgets::Paragraph::new("[c]lock pulse | [t]rim history | [q]uit")
                                .alignment(ratatui::layout::Alignment::Center)
                                .style(TEXT_STYLE);
                        frame.render_widget(info_widget, root_layout[1]);
                        let content_layout = Layout::default()
                            .direction(Direction::Horizontal)
                            .constraints(vec![Constraint::Min(16), Constraint::Max(40)])
                            .split(root_layout[0]);
                        // Populate memory view
                        let mut history_state =
                            ListState::default().with_selected(Some(*active_history));
                        let mut prev_instruction_cycles = 0;
                        let history_widget = List::new(
                            memory_history
                                .iter()
                                .enumerate()
                                .map(|(i, d)| {
                                    let opcode_name: &str;
                                    if prev_instruction_cycles == 0 {
                                        prev_instruction_cycles = OPCODE_CYCLES[d.0 as usize];
                                        opcode_name = OPCODE_NAMES[d.0 as usize];
                                    } else {
                                        opcode_name = " \" "
                                    }
                                    prev_instruction_cycles =
                                        prev_instruction_cycles.saturating_sub(1);
                                    ListItem::new(format!(
                                        "{:04}: {:04X} {:02X}  ({})",
                                        i, d.1, d.0, opcode_name
                                    ))
                                })
                                .collect::<Vec<_>>(),
                        )
                        .style(TEXT_STYLE)
                        .block(
                            Block::default()
                                .borders(Borders::ALL)
                                .border_set(symbols::border::ROUNDED)
                                .style(BLOCK_STYLE)
                                .title("Bus History"),
                        )
                        .highlight_style(HIGHLIGHT_STYLE)
                        .highlight_symbol(">");
                        frame.render_stateful_widget(
                            history_widget,
                            content_layout[0],
                            &mut history_state,
                        );
                        let history_scrollbar =
                            Scrollbar::new(widgets::ScrollbarOrientation::VerticalRight);
                        let mut scrollbar_state = ScrollbarState::new(memory_history.iter().len())
                            .position(*active_history);
                        frame.render_stateful_widget(
                            history_scrollbar,
                            content_layout[0].inner(&Margin {
                                vertical: 1,
                                horizontal: 1,
                            }), // using a inner vertical margin of 1 unit makes the scrollbar inside the block
                            &mut scrollbar_state,
                        );
                        // Populate details
                        let detail_widget = widgets::Paragraph::new("Not yet implemented")
                            .block(
                                Block::default()
                                    .borders(Borders::ALL)
                                    .border_set(symbols::border::ROUNDED)
                                    .style(BLOCK_STYLE)
                                    .title("More Details"),
                            )
                            .style(TEXT_STYLE);
                        frame.render_widget(detail_widget, content_layout[1]);
                    }
                    _ => {}
                }
            })
            .unwrap();
    }
}

impl Drop for MOSTrapTUI {
    fn drop(&mut self) {
        if self.needs_cleanup {
            stdout()
                .execute(LeaveAlternateScreen)
                .expect("Error leaving alternate screen");
            terminal::disable_raw_mode().expect("Error disabling raw mode");
        }
    }
}
