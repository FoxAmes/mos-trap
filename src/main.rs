mod hal;
mod mos;
mod ui;

use mos::MOS6502;
use std::{sync::mpsc::TryRecvError, time::Duration};

#[cfg(feature = "hal-rpi")]
use hal::rpi::HalData;
use ui::{MOSTrapTUI, TUIInputEvent, TUIState};

#[cfg(feature = "hal-rpi")]
type Pin = rppal::gpio::IoPin;

/// Initialize the MOS driver
fn init_mos() -> Result<MOS6502<Pin>, anyhow::Error> {
    let hal_data = HalData::new().unwrap();
    Ok(mos::MOS6502::new(
        hal_data.get_addr_pins()?,
        hal_data.get_data_pins()?,
        hal_data.get_clock_pin()?,
    ))
}

fn main() -> anyhow::Result<()> {
    let mut mos = init_mos()?;

    let (tui, tui_render_handle, events_out) = MOSTrapTUI::new()?;

    while !tui.lock().unwrap().should_close {
        std::thread::sleep(Duration::from_millis(10));
        match events_out.try_recv() {
            Ok(TUIInputEvent::Clock) => {
                let mut state = &mut tui
                    .lock()
                    .or(Err(anyhow::anyhow!("Error locking UI to pulse clock")))?
                    .state;
                mos.pulse_clock()?;
                let addr = mos.read_addr_bus()?;
                let data = mos.read_data_bus()?;
                if let TUIState::MemoryView {
                    memory_history,
                    active_history,
                } = &mut state
                {
                    memory_history.push((data, addr));
                    *active_history = memory_history.len() - 1;
                }
            }
            Err(TryRecvError::Empty) => {}
            Err(_) => anyhow::bail!("Event channel closed"),
        }
    }

    tui_render_handle.join().unwrap()?;
    Ok(())
}
