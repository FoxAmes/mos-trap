// Reference: https://www.masswerk.at/6502/6502_instruction_set.html

#[rustfmt::skip]
pub const OPCODE_NAMES: [&str; 256] = [
    "BRK", "ORA", "JAM", "SLO", "NOP", "ORA", "ASL", "SLO", "PHP", "ORA", "ASL", "ANC", "NOP", "ORA", "ASL", "SLO",
    "BPL", "ORA", "JAM", "SLO", "NOP", "ORA", "ASL", "SLO", "CLC", "ORA", "NOP", "SLO", "NOP", "ORA", "ASL", "SLO",
    "JSR", "AND", "JAM", "RLA", "BIT", "AND", "ROL", "RLA", "PLP", "AND", "ROL", "ANC", "BIT", "AND", "ROL", "RLA",
    "BMI", "AND", "JAM", "RLA", "NOP", "AND", "ROL", "RLA", "SEC", "AND", "NOP", "RLA", "NOP", "AND", "ROL", "RLA",
    "RTI", "EOR", "JAM", "SRE", "NOP", "EOR", "LSR", "SRE", "PHA", "EOR", "LSR", "ALR", "JMP", "EOR", "LSR", "SRE",
    "BVC", "EOR", "JAM", "SRE", "NOP", "EOR", "LSR", "SRE", "CLI", "EOR", "NOP", "SRE", "NOP", "EOR", "LSR", "SRE",
    "RTS", "ADC", "JAM", "RRA", "NOP", "ADC", "ROR", "RRA", "PLA", "ADC", "ROR", "ARR", "JMP", "ADC", "ROR", "RRA",
    "BVS", "ADC", "JAM", "RRA", "NOP", "ADC", "ROR", "RRA", "SEI", "ADC", "NOP", "RRA", "NOP", "ADC", "ROR", "RRA",
    "NOP", "STA", "NOP", "SAX", "STY", "STA", "STX", "SAX", "DEY", "NOP", "TXA", "ANE", "STY", "STA", "STX", "SAX",
    "BCC", "STA", "JAM", "SHA", "STY", "STA", "STX", "SAX", "TYA", "STA", "TXS", "TAS", "SHY", "STA", "SHX", "SHA",
    "LDY", "LDA", "LDX", "LAX", "LDY", "LDA", "LDX", "LAX", "TAY", "LDA", "TAX", "LXA", "LDY", "LDA", "LDX", "LAX",
    "BCS", "LDA", "JAM", "LAX", "LDY", "LDA", "LDX", "LAX", "CLV", "LDA", "TSX", "LAS", "LDY", "LDA", "LDX", "LAX",
    "CPY", "CMP", "NOP", "DCP", "CPY", "CMP", "DEC", "DCP", "INY", "CMP", "DEX", "SBX", "CPY", "CMP", "DEC", "DCP",
    "BNE", "CMP", "JAM", "DCP", "NOP", "CMP", "DEC", "DCP", "CLD", "CMP", "NOP", "DCP", "NOP", "CMP", "DEC", "DCP",
    "CPX", "SBC", "NOP", "ISC", "CPX", "SBC", "INC", "ISC", "INX", "SBC", "NOP", "USBC", "CPX", "SBC", "INC", "ISC",
    "BEQ", "SBC", "JAM", "ISC", "NOP", "SBC", "INC", "ISC", "SED", "SBC", "NOP", "ISC", "NOP", "SBC", "INC", "ISC",
];

#[rustfmt::skip]
pub const OPCODE_LEGALITY: [bool; 256] = [
    true,  true,  false, false, false, true,  true,  false, true,  true,  true,  false, true,  true,  true,  false,
    true,  true,  false, false, false, true,  true,  false, true,  true,  false, false, false, true,  true,  false,
    true,  true,  false, false, true,  true,  true,  false, true,  true,  true,  false, true,  true,  true,  false,
    true,  true,  false, false, false, true,  true,  false, true,  true,  false, false, false, true,  true,  false,
    true,  true,  false, false, false, true,  true,  false, true,  true,  true,  false, true,  true,  true,  false,
    true,  true,  false, false, false, true,  true,  false, true,  true,  false, false, false, true,  true,  false,
    true,  true,  false, false, false, true,  true,  false, true,  true,  true,  false, true,  true,  true,  false,
    true,  true,  false, false, false, true,  true,  false, true,  true,  false, false, false, true,  true,  false,
    false, true,  false, false, true,  true,  true,  false, true,  true,  true,  false, true,  true,  true,  false,
    true,  true,  false, false, true,  true,  true,  false, true,  true,  true,  false, false, true,  false, false,
    true,  true,  true,  false, true,  true,  true,  false, true,  true,  true,  false, true,  true,  true,  false,
    true,  true,  false, false, true,  true,  true,  false, true,  true,  true,  false, true,  true,  true,  false,
    true,  true,  false, false, true,  true,  true,  false, true,  true,  true,  false, true,  true,  true,  false,
    true,  true,  false, false, false, true,  true,  false, true,  true,  false, false, false, true,  true,  false,
    true,  true,  false, false, true,  true,  true,  false, true,  true,  true,  false, true,  true,  true,  false,
    true,  true,  false, false, false, true,  true,  false, true,  true,  false, false, false, true,  true,  false,
];

#[rustfmt::skip]
pub const OPCODE_WIDTH: [u8; 256] = [
//      0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F
/*0*/   1, 2, 1, 2, 2, 2, 2, 2, 1, 2, 1, 2, 3, 3, 3, 3,
/*1*/   2, 2, 1, 2, 2, 2, 2, 2, 1, 3, 1, 3, 3, 3, 3, 3,
/*2*/   3, 2, 1, 2, 2, 2, 2, 2, 1, 2, 1, 2, 3, 3, 3, 3,
/*3*/   2, 2, 1, 2, 2, 2, 2, 2, 1, 3, 1, 3, 3, 3, 3, 3,
/*4*/   1, 2, 1, 2, 2, 2, 2, 2, 1, 2, 1, 2, 3, 3, 3, 3,
/*5*/   2, 2, 1, 2, 2, 2, 2, 2, 1, 3, 1, 3, 3, 3, 3, 3,
/*6*/   1, 2, 1, 2, 2, 2, 2, 2, 1, 2, 1, 2, 3, 3, 3, 3,
/*7*/   2, 2, 1, 2, 2, 2, 2, 2, 1, 3, 1, 3, 3, 3, 3, 3,
/*8*/   2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 1, 2, 3, 3, 3, 3,
/*9*/   2, 2, 1, 2, 2, 2, 2, 2, 1, 3, 1, 3, 3, 3, 3, 3,
/*A*/   2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 1, 2, 3, 3, 3, 3,
/*B*/   2, 2, 1, 2, 2, 2, 2, 2, 1, 3, 1, 3, 3, 3, 3, 3,
/*C*/   2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 1, 2, 3, 3, 3, 3,
/*D*/   2, 2, 1, 2, 2, 2, 2, 2, 1, 3, 1, 3, 3, 3, 3, 3,
/*E*/   2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 1, 2, 3, 3, 3, 3,
/*F*/   2, 2, 1, 2, 2, 2, 2, 2, 1, 3, 1, 3, 3, 3, 3, 3,
];

#[rustfmt::skip]
pub const OPCODE_CYCLES: [u8; 256] = [
//      0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F
/*0*/   7, 6, 0, 8, 0, 3, 5, 5, 3, 2, 2, 2, 0, 4, 6, 6,
/*1*/   2, 5, 0, 8, 0, 4, 6, 6, 2, 4, 0, 7, 0, 4, 7, 7,
/*2*/   6, 6, 0, 8, 3, 3, 5, 5, 4, 2, 2, 2, 4, 4, 6, 6,
/*3*/   2, 5, 0, 8, 0, 4, 6, 6, 2, 4, 0, 7, 0, 4, 7, 7,
/*4*/   6, 6, 0, 8, 0, 3, 5, 5, 3, 2, 2, 2, 3, 4, 6, 6,
/*5*/   2, 5, 0, 8, 0, 4, 6, 6, 2, 4, 0, 7, 0, 4, 7, 7,
/*6*/   6, 6, 0, 8, 0, 3, 5, 5, 4, 2, 2, 2, 5, 4, 6, 6,
/*7*/   2, 5, 0, 8, 0, 4, 6, 6, 2, 4, 0, 7, 0, 4, 7, 7,
/*8*/   0, 6, 0, 6, 3, 3, 3, 3, 2, 0, 2, 2, 4, 4, 4, 4,
/*9*/   2, 6, 0, 6, 4, 4, 4, 4, 2, 5, 2, 5, 5, 5, 5, 5,
/*A*/   2, 6, 2, 6, 3, 3, 3, 3, 2, 2, 2, 2, 4, 4, 4, 4,
/*B*/   2, 5, 0, 5, 4, 4, 4, 4, 2, 4, 2, 4, 4, 4, 4, 4,
/*C*/   2, 6, 0, 8, 3, 3, 5, 5, 2, 2, 2, 2, 4, 4, 6, 6,
/*D*/   2, 5, 0, 8, 0, 4, 6, 6, 2, 4, 0, 7, 0, 4, 7, 7,
/*E*/   2, 6, 0, 8, 3, 3, 5, 5, 2, 2, 2, 2, 4, 4, 6, 6,
/*F*/   2, 5, 0, 8, 0, 4, 6, 6, 2, 4, 0, 7, 0, 4, 7, 7
];
