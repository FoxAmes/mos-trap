pub mod opcodes;

use std::{thread, time::Duration};

use embedded_hal::digital::{v2::InputPin, v2::IoPin, v2::OutputPin};

pub const ADDR_WIDTH: usize = 16;
pub const DATA_WIDTH: usize = 8;

pub struct MOS6502<T>
where
    T: IoPin<T, T> + InputPin + OutputPin,
{
    addr_bus: [T; ADDR_WIDTH],
    data_bus: [T; DATA_WIDTH],
    clock: T,
}

impl<T> MOS6502<T>
where
    T: IoPin<T, T> + InputPin + OutputPin,
{
    pub fn new(addr_bus: [T; ADDR_WIDTH], data_bus: [T; DATA_WIDTH], clock: T) -> Self {
        Self {
            addr_bus,
            data_bus,
            clock,
        }
    }

    pub fn read_addr_bus(&mut self) -> Result<u16, anyhow::Error> {
        let mut value = 0;
        for (i, p) in self.addr_bus.iter().enumerate() {
            if p.is_high()
                .or(Err(anyhow::anyhow!("Error checking pin state.")))?
            {
                value |= 1 << i;
            }
        }
        Ok(value)
    }

    pub fn read_data_bus(&mut self) -> Result<u8, anyhow::Error> {
        let mut value = 0;
        for (i, p) in self.data_bus.iter_mut().enumerate() {
            if p.is_high()
                .or(Err(anyhow::anyhow!("Error checking pin state.")))?
            {
                value |= 1 << i;
            }
        }
        Ok(value)
    }

    pub fn pulse_clock(&mut self) -> Result<(), anyhow::Error> {
        self.clock
            .set_high()
            .or(Err(anyhow::anyhow!("Error setting clock pin high")))?;
        thread::sleep(Duration::from_millis(1));
        self.clock
            .set_low()
            .or(Err(anyhow::anyhow!("Error setting clock pin low")))?;

        Ok(())
    }
}
